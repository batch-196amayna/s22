console.log("Hello World");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/


function register(toRegister){
    let tobeRegister = registeredUsers.includes(toRegister);
	if(tobeRegister){
		alert("Registration failed. Username already exists!");
	} else {
		registeredUsers.push(toRegister);
		alert("Thank you for registering!");
		
	}
}


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/
function addFriend(toAdd){
    let foundUser = registeredUsers.includes(toAdd);
	if(foundUser){
       alert("User not found.");
	} else {
        friendsList.push(toAdd);
        alert("You have added " + toAdd + " as a friend!");
	}
}



/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.

*/
function friendListChecker(){

    if(friendsList.length > 0){
        friendsList.forEach(function(friend){
        console.log(friend);
    })
       
    } else {
        console.log("You currently have" + friendsList.length + " friends. Add one first.");
    }
}



/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.

*/
function friendChecker(){

    if(friendsList.length === 0){
        alert("You currently have" + friendsList.length + " friends.")
       
    } else {
        console.log("You have" + friendsList.length + " friends. Add one first.");
    }
}
/*
    5. Create a function which will delete the last item you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.

*/
function deleteLastFriend(){
    if(friendsList.length=== 0){
        alert("You currently have" + friendsList.length + " friends. Add one first.")
    } else {
        friendsList.pop();
    }
}



//Alternate Activity--------------------------------------------------------------------------

let sentence = ["My", "favorite", "fast food", "is", "Burger King"]

let fastfood = sentence.join(" ");
    console.log(fastfood);


let roles = ["Paladin", "Priest", "Engineer", "Assasin", "Thief"]
console.log(roles);
function deleteLastItem(){
    if(roles.length=== 0){
        alert("No roles available");
    } else {
        roles.pop();
        alert("role has been deleted");
    }
}
deleteLastItem();
console.log(roles);

function addItem(toAdd){
    let newRole = roles.includes(toAdd);
    if(newRole){
       alert("Role already added.");
    } else {
        roles.push(toAdd);
        alert("Role added. There are " + roles.length + " roles available");
        console.log(roles);
    }
}
